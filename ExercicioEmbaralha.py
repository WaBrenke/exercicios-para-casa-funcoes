def embaralha(): #Defini a função embaralha
    import random #Importa a biblioteca random
    palavra = input("Digite uma palavra: ") #Armazena a palavra
    embaralhador = random.sample(palavra, len(palavra))  #Embaralha a palavra, mas retorna uma lista
    palavra_embaralhada = ''.join(embaralhador) #Transforma a lista em string
    return palavra_embaralhada.upper() #pode usar o .lower() também (Altera o formato das letras)
print(embaralha())
